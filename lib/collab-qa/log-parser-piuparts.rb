module CollabQA
  class Log
    def guess_failed_piuparts
      if @data !~ /DC-Piuparts-Status: /
        @reasons = ["TIMEOUT"]
        return
      elsif @data =~ /WARNING: The following essential packages will be removed./
        @reasons = ["CONFLICTS_ESSENTIAL"]
        return
      elsif @data =~ /ERROR: Command failed .* 'apt-get', '-y', 'install'/
        @reasons = ["INST_FAILED"]
        return
      else
        puts "Uknown reason"
        exit(1)
      end
    end

    def extract_log_piuparts
      @sum_1l = "XXX"
      if @reasons == ["INST_FAILED"]
        i = @lines.grep_index(/ERROR: Command failed .* 'apt-get', '-y', 'install'/)[0]
        i2 = @lines[i..-1].grep_index(/ DEBUG:/)[0]
        @extract = @lines[(i+1)..(i+i2-2)]
        # first case: we have a Setting up
        if (t = @extract.grep_index(/^  Setting up /)).length > 0
          i = t[-1]
          @sum_ml = @extract[i..-1]
        elsif (t = @extract.grep_index(/^  Unpacking /)).length > 0
          i = t[-1]
          @sum_ml = @extract[i..-1]
        else
          puts "unknown case"
          exit(1)
        end
      elsif @reasons == ['CONFLICTS_ESSENTIAL']
        i = @lines.grep_index(/ERROR: Command failed .* 'apt-get', '-y', 'install'/)[0]
        i2 = @lines[i..-1].grep_index(/ DEBUG:/)[0]
        @extract = @lines[i..(i+i2)]
        @sum_ml = @extract
      elsif @reasons == ["TIMEOUT"]
        @extract = @lines[-5..-1]
        @sum_ml = @extract
      else
        puts "unknown case2"
        exit(1)
      end
    end
  end
end
