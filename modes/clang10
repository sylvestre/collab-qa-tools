#!/bin/bash
# To run it:
# copy this file to the tmp/ of the chroot
# sbuild xxx.dsc  -d unstable -A  --chroot-setup-commands=/tmp/clang10
set -x
echo "Entering customization script..."
CLANG_VERSION="10"

echo "Install of clang-$CLANG_VERSION"
apt-get update
apt-get install --yes --no-install-recommends --force-yes clang-$CLANG_VERSION libomp-$CLANG_VERSION-dev libobjc-9-dev

echo "Replace gcc, g++ & cpp by clang"
VERSIONS="4.6 4.7 4.8 4.9 5 6 7 8 9 10"
cd /usr/bin
for VERSION in $VERSIONS; do
    rm -f g++-$VERSION gcc-$VERSION cpp-$VERSION gcc
    ln -s clang++-$CLANG_VERSION g++-$VERSION
    ln -s clang-$CLANG_VERSION gcc-$VERSION
    ln -s clang-$CLANG_VERSION cpp-$VERSION
    ln -s clang-$CLANG_VERSION gcc
    echo "gcc-$VERSION hold"|dpkg --set-selections
    echo "g++-$VERSION hold"|dpkg --set-selections
done
cd -

echo "Check if gcc, g++ & cpp are actually clang"
gcc --version|grep clang > /dev/null || exit 1
cpp --version|grep clang > /dev/null || exit 1
g++ --version|grep clang > /dev/null || exit 1

#### Qmake configuration
cd /usr/bin/
rm -f clang++ clang

# To help qmake
ln -s clang-$CLANG_VERSION clang++
ln -s clang-$CLANG_VERSION clang

clang++ --version
clang --version
cd -

# Force the configruation of qmake to workaround this issue:
# https://clang.debian.net/status.php?version=9.0.1&key=FAILED_PARSE_DEFAULT
apt install --yes --no-install-recommends --force-yes qt5-qmake
cp /usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-clang/* /usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++/
ls -al /usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++/
cat /usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++/qmake.conf
export QMAKESPEC=/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-clang/

# Clang doesn't generate exactly the same symbols as gcc:
# https://clang.debian.net/status.php?version=9.0.1&key=CHANGE_SYM_LIB
# Reported upstream https://bugs.llvm.org/show_bug.cgi?id=45322
# More info on mangling:
# https://fitzgeraldnick.com/2017/02/22/cpp-demangle.html
# https://itanium-cxx-abi.github.io/cxx-abi/abi.html#mangling
# So, don't break the build in case of new symbols or missing symbols
# But we still fail if a lib is missing or added
sed -i -e "s|compare_problem(2,|compare_problem(0,|g" /usr/bin/dpkg-gensymbols
sed -i -e "s|compare_problem(1,|compare_problem(0,|g" /usr/bin/dpkg-gensymbols
grep "compare_problem(" /usr/bin/dpkg-gensymbols
